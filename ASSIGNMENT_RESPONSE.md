# Assignment response


## Understanding of assignment

> The goal of the assignment is to write a piece of code which allows you to call "/laureate?country=NL" or "/laureate?country=Germany" that returns laureates that are associated with the country

> Option to develop support for country codes, the full country name, or both

No further comments.


## Solution

A new file (`country.js`) has been created to take care of all country related functions. 

The flow of the main function (`adjustCountry`) is as follows:
* Check on existance of variable `country` in the querystring. If not, other paths of the API should work as they did before
* In case the `country` variable does exist, the variable will be replaced by `affiliation`
* In case the value of the `country` variable is a countryCode, a function is called to retrieve all related countries


## (in-)correct working

The application works correctly for country names and unique country codes. However, the application does *NOT* work correctly for country codes having multi country names (1:n relationships) like `DE` and `PL`.

For example country code `DE` has the following country names:
* `Bavaria`
* `East Friesland`
* `East Germany`
* `Germany`
* `Hesse-Kassel`
* `Mecklenburg`
* `Schleswig`
* `West Germany`
* `Württemberg`

Unfortunately the Nobel Prize API does not support querystrings with multiple affiliations. 


## Future development

To re-write the code:
1. to seperately request the Nobel Prize API for each country name
2. to collect all results, summary and return them in one JSON object


## Last comment

I found this a very interesting assignment with many aspects. However, I am way over 2 hours of time to spend, so I hope you understand I will not complete the `futute development` section ;-)

