'use strict';

const Koa       = require('koa');
const request   = require('request-promise-native');
const country   = require('./country');

const PORT      = 4001;
const API_URL   = 'http://api.nobelprize.org/v1/';

const app       = new Koa();



// logger
app.use(async (ctx, next) => {
  await next();
  const rt = ctx.response.get('X-Response-Time');
  console.log(`${ctx.method} ${ctx.url} - ${rt}`);
});


// x-response-time
app.use(async (ctx, next) => {
  const start = Date.now();
  await next();
  const ms = Date.now() - start;
  ctx.set('X-Response-Time', `${ms}ms`);
});


// Proxy
app.use(async (ctx) => {
  var queryString   = '';
  const path        = `${ctx.path.slice(1)}.json`;
  
  if (ctx.querystring.length > 0) {
    queryString   = await country.adjustCountry(ctx.querystring);
    queryString   = `?${queryString}`;
  };

  const url     = `${API_URL}${path}${queryString}`;
  const result  = await request(url);
  const parsed  = JSON.parse(result);

  // console.log('ctx.path:  ', ctx.path);
  // console.log('queryString:\n', queryString);
  // console.log('url:\n', url);
  // console.log(JSON.parse(result));

  ctx.body = parsed;
});



app.listen(PORT);

console.log(`Listening on http://localhost:${PORT}`);
