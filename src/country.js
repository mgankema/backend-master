'use strict';

const request   = require('request-promise-native');



async function getCountryNames(cntryCode) {    
    var array       = [];
    const result    = await request('http://api.nobelprize.org/v1/country.json');
    const parsed    = JSON.parse(result);
    const length    = parsed.countries.length;

    for (var i = 0; i < length; i++) {
        if (parsed.countries[i].code === cntryCode) 
            array.push(parsed.countries[i].name);
    };
    // console.log('array:\n', array);
    return array;
};


function replaceCodeWithNames(stringIn, cntryCode, cntryNames) {
    var string      = stringIn.replace(cntryCode, cntryNames[0]);
    const length    = cntryNames.length;

    if (length > 1) {
        for (var i = 1; i < length; i++) {
            string = string + `&affiliation=${cntryNames[i]}`;
        };
    };

    // console.log('replaceCodeWithNames - string:\n', string);
    return string;
};


async function adjustCountry(queryString) {
    var stringIn        = queryString;
    const urlParams     = new URLSearchParams(stringIn);
    const cntryValue    = urlParams.get('country');
    var stringOut, cntryNames;

    if (cntryValue !== null) {
        if (cntryValue.length === 2) {
            cntryNames  = await getCountryNames(cntryValue);
            stringIn    = replaceCodeWithNames(stringIn, cntryValue, cntryNames);
        };
        stringOut = stringIn.replace('country', 'affiliation');
    } else {
        stringOut = stringIn;
    };
    
    // console.log('stringOut: ', stringOut);
    return stringOut;
};



module.exports = {
    adjustCountry
}
