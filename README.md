# Sensorfact Node.js Back End Developer Test 1

## Assignment

This is a small [Koa](http://koajs.com/)-based REST API that proxies requests
from `localhost:4001/[route]` to `api.nobelprize.org/v1/[route].json` of
[Nobel Prize API](https://nobelprize.readme.io/docs/getting-started). Where
route is one of:

* `country`
* `laureate`
* `prize`

That is you can write a query like `localhost:4001/laureate?gender=female` and it will be proxied to `api.nobelprize.org/v1/laureate.json?gender=female`.

Write support for a new `country` query string argument to the `laureate` route.

*Note:* this argument is _NOT_ passed through to Nobel Prize API but should be used to filter laureates with a prize affiliated with a specific country.

Write your code in `src/index.js`.
